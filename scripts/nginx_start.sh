#!/bin/bash
cp -rvf /tmp/test-codedeploy/* /usr/share/nginx/html/test-codedeploy/
service nginx start
if [ $? != 0 ]
then
echo "service nginx failed to start"
exit 1
fi
