#!/bin/bash

service nginx stop
if [ $? != 0 ]
then
echo "service nginx failed to stop"
exit 1
fi
